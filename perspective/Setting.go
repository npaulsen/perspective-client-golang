package perspective

type Setting struct {
	Value  string `json:"value"`
	Secret bool   `json:"secret"`
}
