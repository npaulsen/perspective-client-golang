package perspective

import (
	"time"
)

type configData struct {
	jsonRequest []byte
	url         string
	cacheTime   time.Duration
}

func (c configData) getURL() string {
	return c.url
}

func (c configData) getRequest() []byte {
	return c.jsonRequest
}

func (c configData) getCacheTime() time.Duration {
	return c.cacheTime
}
