package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"
	"time"
	"perspective"
)

const cacheSettingKey = "perspective-cache-time"

var config configData
var settingMap = make(map[string]Setting)
var mu sync.RWMutex

func main() {
	fmt.Println("Starting the application...")

	var acctKey accountKey

	acctKey.email = "norman.paulsen@gmail.com"
	acctKey.accountId = "92a9aaa7-d396-ea11-86e9-281878880378"

	var configBuild configurationBuilder

	configBuild.account = acctKey
	configBuild.apiKey = "73109e92-046f-4ad8-8f70-3ebdbafa3862"
	configBuild.application = "tamaton"
	configBuild.environment = "PRD"
	configBuild.cacheTime = 1000

	config = configBuild.generate()

	repeat()

	fmt.Println("Terminating the application...")
}

func getSetting(key string) string {
	mu.RLock()
	var val = settingMap[key].Value
	mu.RUnlock()
	return val
}

func repeat() {
	for i := 0; i < 3; i++ {
		response, err := http.Post(config.getURL(), "application/json", bytes.NewBuffer(config.getRequest()))
		if err != nil {
			fmt.Printf("The HTTP request failed with error %s\n", err)
		} else {
			data, _ := ioutil.ReadAll(response.Body)

			var tempMap = make(map[string]Setting)

			err := json.Unmarshal(data, &tempMap)

			mu.Lock()
			settingMap = tempMap
			mu.Unlock()

			fmt.Println(string(data))

			fmt.Printf("%v", settingMap["1"])

			if err != nil {
				panic(err)
			}

			fmt.Printf("%v", settingMap["perspective-professional-id-monthly"])
		}

		time.Sleep(getCacheTime() * time.Millisecond)

		fmt.Println(i)
	}
}

func getCacheTime() time.Duration {
	var cacheSetting = getSetting(cacheSettingKey)
	if cacheSetting != "" {
		parsedTime, err := time.ParseDuration(cacheSetting + "ms")
		if err == nil {
			return parsedTime
		}
	}
	return config.cacheTime
}
