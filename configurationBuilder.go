package perspective

import "time"

type accountKey struct {
	accountId string
	email     string
}

type configurationBuilder struct {
	account     accountKey
	apiKey      string
	application string        `default:""`
	environment string        `default:""`
	instance    string        `default:""`
	cacheTime   time.Duration `default:"600000"`
}

func (c configurationBuilder) generate() configData {
	var config configData
	config.jsonRequest = c.generateJsonRequest()
	if c.instance != "" {
		c.instance = c.instance + "."
	}
	config.url = "https://" + c.instance + "portal.tamaton.com/perspective/api/get-parameters.do"
	config.cacheTime = c.cacheTime

	return config
}

func (c configurationBuilder) generateJsonRequest() []byte {
	return []byte("{" +
		"\"accountKey\": {" +
		"\"accountId\":\"" + c.account.accountId + "\"," +
		"\"email\":\"" + c.account.email + "\"" +
		"}," +
		"\"apiKey\":\"" + c.apiKey + "\"," +
		"\"application\":\"" + c.application + "\"," +
		"\"environment\":\"" + c.environment + "\"" +
		"}")
}
