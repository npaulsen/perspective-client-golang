package perspective

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"
	"time"
	"perspective"
)

const cacheSettingKey = "perspective-cache-time"

var config configData
var settingMap = make(map[string]Setting)
var mu sync.RWMutex

func getSetting(key string) string {
	mu.RLock()
	var val = settingMap[key].Value
	mu.RUnlock()
	return val
}